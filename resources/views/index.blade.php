<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<!--====== Title ======-->
	<title>LeMESSAGER</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/js/all.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!--====== Favicon Icon ======-->
	<link rel="shortcut icon" href=" {{asset('assets/images/logo.png')}} " type="image/png">
	<!--====== Bootstrap CSS ======-->
	<link rel="stylesheet" href=" {{asset('assets/css/bootstrap-5.0.5-alpha.min.css')}} ">

	<!--====== Style CSS  -p-- ======-->
	<link rel="stylesheet" href=" {{asset('assets/css/style.css')}} ">
</head>
<body>
	<!--====== PRELOADER PART START ======-->
     <div class="preloader">
		<div class="loader">
			<div class="ytp-spinner">
				<div class="ytp-spinner-container">
					<div class="ytp-spinner-rotator">
						<div class="ytp-spinner-left">
							<div class="ytp-spinner-circle"></div>
						</div>
						<div class="ytp-spinner-right">
							<div class="ytp-spinner-circle"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
	<!--====== PRELOADER PART ENDS ======-->

	<!--====== HEADER PART START ======-->
	<header class="header_area">
		<div id="header_navbar" class="header_navbar">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-xl-12">
						<nav class="navbar navbar-expand-lg">
							<a class="navbar-brand">
								<img id="logo" src=" {{asset('assets/images/messager.jpg')}} " alt="Logo">
							</a> 
							<div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
								<ul id="nav" class="ml-auto navbar-nav">
									<li class="nav-item" >
										<a class="page-scroll" href="#Nos Offres">Nos Offres</a>
									</li>
								</ul>
							</div> <!-- navbar collapse -->
					</div>
				</div> <!-- row -->
			</div> <!-- container -->
		</div> <!-- header navbar -->
	</header>
	<!--====== HEADER PART ENDS ======--> 
	<div class="">
		<!--======SLIDER START ======-->
		<div class="container">
	   		 <div class="slider-container"  style="border: #000 solid 1px;">
				 <div class="menu">
					<label for="slide-dot-1"></label>
					<label for="slide-dot-2"></label>
					<label for="slide-dot-3"></label>
				 </div>
					<input class="slide-input" id="slide-dot-1" type="radio" name="slides" checked>
					<video class="slide-img"   src="{{asset('assets/video/video-bg.mp4')}}" alt="First slide" autoplay controls loop>
					</video>
					<input class="slide-input" id="slide-dot-2" type="radio" name="slides">
					<img class="slide-img" src="https://www.codeur.com/tuto/wp-content/uploads/2021/12/slide2.jpg">
					<input class="slide-input" id="slide-dot-3" type="radio" name="slides">
					<img class="slide-img" src="https://www.codeur.com/tuto/wp-content/uploads/2021/12/slide3.jpg">
			 </div>
		 {{-- <div class="card" style="border: solid 1px;"> 
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" >
					<ol class="carousel-indicators">
						<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					 </ol>
					<div class="carousel-inner" style="height:30rem;" id="pageResp">
						<div class="carousel-item active">
							<video class="owl-carousel owl-theme" id="video_1" src="{{asset('assets/video/video-bg.mp4')}}" alt="First slide" autoplay controls width="100%">
							</video>
					  	</div>
					  <div class="carousel-item">
							<img class="d-block w-100" src="{{asset('assets/images/blog-2.jpg')}}" alt="Second slide">
						</div>
						<div class="carousel-item">
							<img class="d-block w-100" src="{{asset('assets/images/blog-3.jpg')}}" alt="Third slide">
						</div>
						<div class="carousel-caption d-none d-md-block">
							<h5>le Monde de nos jours</h5>
							<p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse soluta</p>
						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					  <span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					  <span class="carousel-control-next-icon" aria-hidden="true"></span>
					  <span class="sr-only">Next</span>
					</a>
				  </div> 
			</div>  --}}
		</div><br>
			  <!--======SLIDER END ======-->
 
	   <!--======INFOS-ICONS START ======-->
	
	   <center>
		 <!--===== Debut nombre des Visiteurs  ======-->
	 @php		  
			$ip = fopen('last_ip.txt','c+');
			$check=fgets($ip);
		
			$file = fopen('counter.txt','c+');
			$count = intval(fgets($file));

    	  if ($_SERVER['REMOTE_ADDR'] != $check) {

			fclose($ip);
			$ip =  fopen('last_ip.txt','w+');


			fputs($ip, $_SERVER['REMOTE_ADDR']);
			$count++;
			fseek($file,0);
			fputs($file, $count);
  	   }

			fclose($file);
			fclose($ip);
		
     @endphp
		  <!--====== Fin nombre des Visiteurs ======-->
			<div class="container">
			
				<div>
					<h5 style="color:#000; font-family:'Times New Roman', Times, serif"> Acro-Univ : le nom de l'Université </h5>
				</div>
			
			 <br>
				<div class="row justify-content-md-center" id="center">
				  <div class="col col-lg-2">
				   		5 <i class="fas fa-mobile-alt"></i>
				  </div>
				  <div class="col col-lg-2">
						5 <i class="far fa-envelope"></i>
				  </div>
				  <div class="col col-lg-2">
					@php
					echo $count;	
					@endphp 	<i class="far fa-eye"></i>
				  </div>
				  <div class="col col-lg-2">
				  		 5 <i class="far fa-comment"></i>
				  </div> 
				</div>
			  </div>
		</center>	 
	  <!--======INFOS-ICONS END ======-->
			<div class="container">
				<hr>
				<div class="row justify-content-md-center">
					<div class="choix" style="">
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
							<i class="fas fa-share-alt"></i> Partagez 
						</button> 
						<button type="button" class="btn btn-primary"  data-toggle="modal"  data-target="#exampleModalCenters"> 
						    <i class="fas fa-user"></i> Contacts
						</button>
						<button type="button" class="btn btn-primary"  data-toggle="modal"  data-target="#exampleModal"> 
							<i class="fas fa-clock"></i> RDV
						</button>
					  </div>
				</div> 
				<hr>
				<div class="titre" id="Nos Offres">
					<div class="card">
						<div class="card-header">
							<h5>Nos Offres</h5>
						</div>
						<div class="card-body">
						  <h5 class="card-tite font-weight-bold" style="font-family: 'Times New Roman', Times, serif;">Special title treatment</h5>
						  <p class="card-text">With supporting text below as a natural lead-in to additional content.
							  Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae ipsa ipsum quibusdam esse dolores facilis in numquam animi, facere repudiandae 
							  tempora quis minus distinctio sequi velit laborum eum aperiam labore? Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati maxime sapiente voluptate reiciendis autem consectetur iusto nostrum perferendis aspernatur unde, blanditiis reprehenderit,
							   ut nulla excepturi hic, rem nihil sunt aut.
						  </p>
							 <a href="https://lemessager.suisco.net" class="btn btn-primary btn-sm">Visiter Nôtre Site</a>
						</div>
					  </div>
				</div>				
			</div>
			
	  </div>
	<!--====== SLIDER ENDS ======-->

	<!--====== Bootstrap js ======-->
	<script src=" {{asset('assets/js/bootstrap.bundle-5.0.0.alpha-min.js')}} "></script>

	<!--====== Tiny slider js ======-->
	<script src=" {{asset('assets/js/tiny-slider.js')}} "></script>

	<!--====== wow js ======-->
	<script src=" {{asset('assets/js/wow.min.js')}} "></script>

	<!--====== glightbox js ======-->
	<script src=" {{asset('assets/js/glightbox.min.js')}} "></script>

	<!--====== contact-form js ======-->
	<script src=" {{asset('assets/js/contact-form.js')}} "></script>

	<!--====== Main js ======-->
	<script src=" {{asset('assets/js/main.js')}} "></script>
	<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
 {{-- scrpit ppour effectuer le copy --}}
 <script src="//cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.4.0/clipboard.min.js"></script>
	<script>
		//========= glightbox
		const myGallery = GLightbox({
			'href': 'assets/video/Free App Landing Page Template - AppLand.mp4',
			'type': 'video',
			'source': 'youtube', //vimeo, youtube or local
			'width': 900,
			'autoplayVideos': true,
		});

		//======== tiny slider for testimonial
		tns({
			slideBy: 'page',
			autoplay: false,
			mouseDrag: true,
			gutter: 0,
			nav: true,
			controls: true,
			controlsPosition: 'bottom',
			controlsText: ['<i class="lni lni-chevron-left"></i>', '<i class="lni lni-chevron-right"></i>'],
			"container": "#customize",
			"items": 1,
			"center": true,
			"navContainer": "#customize-thumbnails",
			"navAsThumbnails": true,
			"autoplayTimeout": 5000,
			"swipeAngle": false,
			"speed": 400
		});

    // Get the navbar


    // for menu scroll
    var pageLink = document.querySelectorAll('.page-scroll');

    pageLink.forEach(elem => {
        elem.addEventListener('click', e => {
            e.preventDefault();
            document.querySelector(elem.getAttribute('href')).scrollIntoView({
                behavior: 'smooth',
                offsetTop: 1 - 60,
            });
        });
    });

    // section menu active
    function onScroll(event) {
     var sections = document.querySelectorAll('.page-scroll');
        var scrollPos = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;

        for (var i = 0; i < sections.length; i++) {
            var currLink = sections[i];
            var val = currLink.getAttribute('href');
            var refElement = document.querySelector(val);
            var scrollTopMinus = scrollPos + 73;
            if (refElement.offsetTop <= scrollTopMinus && (refElement.offsetTop + refElement.offsetHeight > scrollTopMinus)) {
                document.querySelector('.page-scroll').classList.remove('active');
                currLink.classList.add('active');
            } else {
                currLink.classList.remove('active');
            }
        }
    };

    window.document.addEventListener('scroll', onScroll);


    //===== close navbar-collapse when a  clicked
    let navbarToggler = document.querySelector(".navbar-toggler");
    var navbarCollapse = document.querySelector(".navbar-collapse");

    document.querySelectorAll(".page-scroll").forEach(e =>
        e.addEventListener("click", () => {
            navbarToggler.classList.remove("active");
            navbarCollapse.classList.remove('show')
        })
    );
    navbarToggler.addEventListener('click', function() {
        navbarToggler.classList.toggle("active");
    });
		 // script pour copier le liens
		 (function(){
         new Clipboard('#copy-button');
    })();




	</script>
 <!-- Modal Partager Debuts -->

 <!-- Button trigger modal -->  
  <!-- Modal -->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLongTitle">Partager</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
		    <center>
					<a class="btn btn-outline-dark" href="https://web.whatsapp.com/">
						<i class="fab fa-whatsapp"style="color: #25D366;font-size:25px"></i>
					</a> &nbsp; &nbsp;&nbsp;
					<a class="btn btn-outline-dark" href="https://webk.telegram.org/" class="share telegram" data-network="telegram">
							<i class="fab fa-telegram" style="color:#0088CC;font-size:25px"></i>
						</a>
						&nbsp; &nbsp;&nbsp;
						<a class="btn btn-outline-dark" href="https://www.instagram.com/" class="share instagram" data-network="instagram"><i
							class="fab fa-instagram" style="color:#E1306C;font-size:25px"></i>
					</a>
						&nbsp; &nbsp;&nbsp;
					<a class="btn btn-outline-dark" href="https://twitter.com/" title="Twitter" target="_blank" class="share twitter"
						data-network="twitter"><i class="fab fa-twitter" style="color:#00acee;font-size:25px"></i>
					</a>
		    </center>
		</div>
		<div class="modal-footer">
		    <div class="input-group mb-3" style="position: relative;top:18px;">
				<input type="text" class="form-control" id="post-shortlink"  value="http://192.168.1.64:8000" aria-label="Recipient's username" aria-describedby="basic-addon2">
			<div class="input-group-append">
			  <button class="btn btn-outline-dark" id="copy-button" data-clipboard-target="#post-shortlink">
			  <i class='fa fa-copy' style="color:red;font-size:22px;"></i>
			  </button>
			</div>
			</div>
		</div>
	  </div>
	</div>
  </div>

  <!-- Modal Partager Fin -->

  <!-- Modal Contactez nous debut -->
  <div class="modal fade" id="exampleModalCenters" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLongTitle">Contactez-Nous</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
		 	  Ajout des Smart  links <br>
		   <a class="btn btn-outline-dark" href="https://wa.me/+22893495167">
				<i class="fab fa-whatsapp"style="color: #25D366;font-size:25px"></i>
		    </a>	
			<a class="btn btn-outline-dark" href="https://telegram.me/+22899540525" class="share telegram" data-network="telegram">
				<i class="fab fa-telegram" style="color:#0088CC;font-size:25px"></i>
		</a>
		<a class="btn btn-outline-dark" href="https://twitter.com/" title="Twitter" target="_blank" class="share twitter"
					data-network="twitter"><i class="fab fa-twitter" style="color:#00acee;font-size:25px"></i>
				</a>
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		</div>
	  </div>
	</div>
  </div>
   <!-- Modal Contactez nous Fin -->

		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Demande de Rendez-Vous</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
				</div>
				<form method="post" action="{{route('TestMail')}}">
					@csrf
				<div class="modal-body">
					<h5>Nous sommes entièrement disponible pour vous entretenir sur l'orientation</h5>
				
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">NOM :</label>
						<input type="text" name="nom" class="form-control" id="recipient-name" placeholder="Renseignez votre Nom">
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Email ou Numero de téléphone :</label>
						<input type="email" name="email" class="form-control" id="recipient-name" placeholder="Email ou Numéro de téléphone">
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Date :</label>
						<input type="date" class="form-control" id="recipient-name">
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Objectifs :</label>
						<input type="text"  name="subject" class="form-control" id="recipient-name" placeholder="Objectifs">
					</div>
					<div class="form-group">
						<label for="message-text" class="col-form-label">Message :</label>
						<textarea  name="message" class="form-control" id="message-text"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Send message</button>
				</div>
			</form>
			</div>
			</div>
		</div>	
 </body>
</html>
