<?php

use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
  // $user = DB::table('users')->select('*')->where('id','=',1)->first();
  // $commentaires = DB::table('commentaires')->select('*')->where('id','=',1)->get();
   //  dd($commentaires);
  //   return view('index',compact('user','commentaires'));
  });

   Route::get('/',[App\Http\Controllers\HomeController::class, 'index'])->name('index');
   
   Route::post('/index',[App\Http\Controllers\MailController::class,'sendEmail'])->name('TestMail'); 

   