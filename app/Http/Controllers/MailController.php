<?php

namespace App\Http\Controllers;

use App\Mail\TestMail;
use Illuminate\Http\Request;
use PHPUnit\Util\Test;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function sendEmail(Request $request)
    {
   
      $details = [
           'nom' => $request->nom,    
           'email' => $request->email,  
           'subject' =>  $request->subject,
           'message' =>  $request->message,
      ];
       Mail::to("tchalimroi@gmail.com")->send(new TestMail($details));
        return Redirect('/');
    }
}
