<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commentaires extends Model
{
    use HasFactory;

    public $table ='commentaires';

    protected $filable = [
        'user_id',
        'message',
    ];


    public function User(){
        return $this-> hasOne('App\Models\User');
    }
}
