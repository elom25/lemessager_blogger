(function() {
    
    "use strict";
    
    //===== Prealoder

    window.onload = function() {
        window.setTimeout(fadeout, 100);
    }

    function fadeout() {
        document.querySelector('.preloader').style.opacity = '0';
        document.querySelector('.preloader').style.display = 'none';
    }

    
    /*=====================================
    Sticky
    ======================================= */
    window.onscroll = function () {
        var header_navbar = document.getElementById("header_navbar");
        var sticky = header_navbar.offsetTop;

        if (window.pageYOffset > sticky) {
            header_navbar.classList.add("sticky");
        } else {
            header_navbar.classList.remove("sticky");
        }



        // show or hide the back-top-top button
        var backToTo = document.querySelector(".back-to-top");
        if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
            backToTo.style.display = "block";
        } else {
            backToTo.style.display = "none";
        }
    };


        //===== close navbar-collapse when a  clicked
    let navbarToggler = document.querySelector(".navbar-toggler");    
    var navbarCollapse = document.querySelector(".navbar-collapse");

    document.querySelectorAll(".page-scroll").forEach(e =>
        e.addEventListener("click", () => {
            navbarToggler.classList.remove("active");
            navbarCollapse.classList.remove('show')
        })
    );
    navbarToggler.addEventListener('click', function() {
        navbarToggler.classList.toggle("active");
    });
    

    //WOW Scroll Spy
    var wow = new WOW({
        //disabled for mobile
        mobile: false
    });
    wow.init();

     //======== tiny slider for work
  tns({
    container: ".work_active",
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayText: [" ", " "],
    mouseDrag: true,
    gutter: 0,
    nav: true,
    controls: false,
    controlsText: [
      '<i class="lni lni-chevron-left prev"></i>',
      '<i class="lni lni-chevron-right next"></i>',
    ],
    items: 5,

    responsive: {
      0: {
        items: 1,
      },
      570: {
        items: 2,
      },
      850: {
        items: 3,
      },
      1200: {
        items: 4,
      },
      1400: {
        items: 5,
      },
    },
  });
  //======== tiny slider for team
  tns({
    container: ".team_active",
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayText: [" ", " "],
    mouseDrag: true,
    gutter: 0,
    nav: true,
    controls: false,
    controlsText: [
      '<i class="lni lni-chevron-left prev"></i>',
      '<i class="lni lni-chevron-right next"></i>',
    ],
    items: 5,

    responsive: {
      0: {
        items: 1,
      },
      768: {
        items: 2,
      },
      1050: {
        items: 3,
      },
    },
  });



})();